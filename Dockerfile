# Build stage
FROM golang:1.20-alpine AS builder
RUN go version
COPY . /go/src/gitlab.com/citaces/rpc-auth
WORKDIR /go/src/gitlab.com/citaces/rpc-auth
RUN go mod download
RUN go build -ldflags="-w -s" -o /go/bin/server /go/src/gitlab.com/citaces/rpc-auth/cmd/api

# Run stage
FROM alpine
COPY --from=builder /go/bin/server /go/bin/server
COPY ./static/swagger.json /app/static/swagger.json
COPY ./.env /app/.env

WORKDIR /app
ENTRYPOINT ["/go/bin/server"]