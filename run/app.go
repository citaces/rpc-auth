package run

import (
	"context"
	"fmt"
	"net/rpc"
	"net/rpc/jsonrpc"
	"os"

	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/citaces/rpc-auth/config"
	"gitlab.com/citaces/rpc-auth/internal/db"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/cache"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/component"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/db/migrate"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/db/scanner"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/errors"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/responder"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/server"
	internal "gitlab.com/citaces/rpc-auth/internal/infrastructure/service"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/tools/cryptography"
	"gitlab.com/citaces/rpc-auth/internal/models"
	"gitlab.com/citaces/rpc-auth/internal/modules"
	"gitlab.com/citaces/rpc-auth/internal/provider"
	"gitlab.com/citaces/rpc-auth/internal/storages"
	"gitlab.com/citaces/rpc-auth/rpc/auth"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	jsonRPC  server.Server
	Sig      chan os.Signal
	Storages *storages.Storages
	Servises *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	//errGroup.Go(func() error {
	//	err := a.srv.Serve(ctx)
	//	if err != nil && err != http.ErrServerClosed {
	//		a.logger.Error("app: server error", zap.Error(err))
	//		return err
	//	}
	//	return nil
	//})

	//запускаем json rpc сервер
	errGroup.Go(func() error {
		err := a.jsonRPC.Serve(ctx)
		if err != nil {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models.UserDTO{},
		&models.EmailVerifyDTO{},
		&models.TestDTO{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}
	// инициализация кэша основанного на redis
	cacheClient, err := cache.NewCache(a.conf.Cache, decoder, a.logger)
	if err != nil {
		a.logger.Fatal("error init cache", zap.Error(err))
	}

	// инициализация хранилищ
	newStorages := storages.NewStorages(sqlAdapter, cacheClient)
	a.Storages = newStorages
	// инициализация сервисов

	userClient, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.UserRPC.Host, a.conf.UserRPC.Port))

	if err != nil {
		a.logger.Fatal("error init rpc client", zap.Error(err))
	}

	services := modules.NewServices(userClient, newStorages, components)
	a.Servises = services

	authRPC := auth.NewAuthServiceJSONRPC(services.Auth)
	jsonRPCServer := rpc.NewServer()
	err = jsonRPCServer.Register(authRPC)
	if err != nil {
		a.logger.Fatal("error init user json RPC", zap.Error(err))
	}
	// инициализация сервера json RPC
	a.jsonRPC = server.NewJSONRPC(a.conf.RPCServer, jsonRPCServer, a.logger)
	return a
}
