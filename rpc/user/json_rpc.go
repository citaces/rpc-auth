package user

import (
	"context"
	"gitlab.com/citaces/rpc-auth/internal/modules/user/service"
)

// UserServiceJSONRPC представляет UserService для использования в JSON-RPC
type UserServiceJSONRPC struct {
	userService service.Userer
}

// NewUserServiceJSONRPC возвращает новый UserServiceJSONRPC
func NewUserServiceJSONRPC(userService service.Userer) *UserServiceJSONRPC {
	return &UserServiceJSONRPC{userService: userService}
}

// CreateUser обрабатывает JSON-RPC запрос на создание пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод CreateUser из userService.
// Возвращает выходные данные.
func (t *UserServiceJSONRPC) CreateUser(in service.UserCreateIn, out *service.UserCreateOut) error {
	*out = t.userService.Create(context.Background(), in)
	return nil
}

// UpdateUser обрабатывает JSON-RPC запрос на обновление пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод Update из userService.
// Возвращает выходные данные.
func (t *UserServiceJSONRPC) UpdateUser(in service.UserUpdateIn, out *service.UserUpdateOut) error {
	*out = t.userService.Update(context.Background(), in)
	return nil
}

// VerifyEmail обрабатывает JSON-RPC запрос на подтверждение email пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод VerifyEmail из userService.
// Возвращает выходные данные.
func (t *UserServiceJSONRPC) VerifyEmail(in service.UserVerifyEmailIn, out *service.UserUpdateOut) error {
	*out = t.userService.VerifyEmail(context.Background(), in)
	return nil
}

// ChangePassword обрабатывает JSON-RPC запрос на смену пароля пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод ChangePassword из userService.
// Возвращает выходные данные.
func (t *UserServiceJSONRPC) ChangePassword(in service.ChangePasswordIn, out *service.ChangePasswordOut) error {
	*out = t.userService.ChangePassword(context.Background(), in)
	return nil
}

// GetUserByEmail обрабатывает JSON-RPC запрос на получение пользователя по email.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод GetByEmail из userService.
// Возвращает выходные данные.
func (t *UserServiceJSONRPC) GetUserByEmail(in service.GetByEmailIn, out *service.UserOut) error {
	*out = t.userService.GetByEmail(context.Background(), in)
	return nil
}

// GetUserByPhone обрабатывает JSON-RPC запрос на получение пользователя по телефону.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод GetByPhone из userService.
// Возвращает выходные данные.
func (t *UserServiceJSONRPC) GetUserByPhone(in service.GetByPhoneIn, out *service.UserOut) error {
	*out = t.userService.GetByPhone(context.Background(), in)
	return nil
}

// GetUserByID обрабатывает JSON-RPC запрос на получение пользователя по ID.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод GetByID из userService.
// Возвращает выходные данные.
func (t *UserServiceJSONRPC) GetUserByID(in service.GetByIDIn, out *service.UserOut) error {
	*out = t.userService.GetByID(context.Background(), in)
	return nil
}

// GetUsersByID обрабатывает JSON-RPC запрос на получение пользователей по списку ID.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод
// GetByIDs из userService. Возвращает выходные данные.
func (t *UserServiceJSONRPC) GetUsersByID(in service.GetByIDsIn, out *service.UsersOut) error {
	*out = t.userService.GetByIDs(context.Background(), in)
	return nil
}
