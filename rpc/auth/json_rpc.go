package auth

import (
	"context"
	"errors"
	aservice "gitlab.com/citaces/rpc-auth/internal/modules/auth/service"
	uservice "gitlab.com/citaces/rpc-auth/internal/modules/user/service"
)

// UserServiceJSONRPC представляет UserService для использования в JSON-RPC
type AuthServiceJSONRPC struct {
	authService aservice.Auther
}

func NewAuthServiceJSONRPC(authService aservice.Auther) *AuthServiceJSONRPC {
	return &AuthServiceJSONRPC{authService: authService}
}

func (a *AuthServiceJSONRPC) Register(in []interface{}, out *aservice.RegisterOut) error {
	n, ok := in[0].(map[string]interface{})
	if !ok {
		return errors.New("AuthServiceJSONRPC Register method error")
	}

	registerIn := aservice.RegisterIn{
		Email:    n["Email"].(string),
		Password: n["Password"].(string),
	}

	field := in[1].(float64)
	*out = a.authService.Register(context.Background(), registerIn, int(field))
	return nil
}

func (a *AuthServiceJSONRPC) AuthorizeEmail(in aservice.AuthorizeEmailIn, out *aservice.AuthorizeOut) error {
	*out = a.authService.AuthorizeEmail(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) AuthorizeRefresh(in aservice.AuthorizeRefreshIn, out *aservice.AuthorizeOut) error {
	*out = a.authService.AuthorizeRefresh(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) AuthorizePhone(in aservice.AuthorizePhoneIn, out *aservice.AuthorizeOut) error {
	*out = a.authService.AuthorizePhone(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) SendPhoneCode(in aservice.SendPhoneCodeIn, out *aservice.SendPhoneCodeOut) error {
	*out = a.authService.SendPhoneCode(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) VerifyEmail(in aservice.VerifyEmailIn, out *aservice.VerifyEmailOut) error {
	*out = a.authService.VerifyEmail(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) SetUserer(in uservice.Userer) {
	a.authService.SetUserer(in)
}
