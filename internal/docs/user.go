package docs

import (
	ucontroller "gitlab.com/citaces/rpc-auth/internal/modules/user/controller"
)

// swagger:route GET /api/1/user/profile user profileRequest
// Получение информации о текущем пользователе.
// security:
//   - Bearer: []
// responses:
//   200: profileResponse

// swagger:response profileResponse
//
//nolint:all
type profileResponse struct {
	// in:body
	Body ucontroller.ProfileResponse
}
