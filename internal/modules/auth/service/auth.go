package service

import (
	"context"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"gitlab.com/citaces/rpc-auth/config"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/component"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/errors"
	iservice "gitlab.com/citaces/rpc-auth/internal/infrastructure/service"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/tools/cryptography"
	"gitlab.com/citaces/rpc-auth/internal/models"
	"gitlab.com/citaces/rpc-auth/internal/modules/auth/storage"
	uservice "gitlab.com/citaces/rpc-auth/internal/modules/user/service"
	"go.uber.org/zap"
)

type Auth struct {
	conf         config.AppConf
	user         uservice.Userer
	verify       storage.Verifier
	notify       iservice.Notifier
	tokenManager cryptography.TokenManager
	hash         cryptography.Hasher
	logger       *zap.Logger
}

func NewAuth(user *uservice.UserServiceJSONRPC, verify storage.Verifier, components *component.Components) *Auth {
	return &Auth{conf: components.Conf,
		user:         user,
		verify:       verify,
		notify:       components.Notify,
		tokenManager: components.TokenManager,
		hash:         components.Hash,
		logger:       components.Logger,
	}
}

func (a *Auth) SetUserer(user uservice.Userer) {
	a.user = user
}

func (a *Auth) Register(ctx context.Context, in RegisterIn, field int) RegisterOut {
	hashPass, err := cryptography.HashPassword(in.Password)
	if err != nil {
		return RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: errors.HashPasswordError,
		}
	}

	userCreate := uservice.UserCreateIn{
		Email:    in.Email,
		Password: hashPass,
	}

	userOut := a.user.Create(ctx, userCreate)
	if userOut.ErrorCode != errors.NoError {
		if userOut.ErrorCode == errors.UserServiceUserAlreadyExists {
			return RegisterOut{
				Status:    http.StatusConflict,
				ErrorCode: userOut.ErrorCode,
			}
		}
		return RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: userOut.ErrorCode,
		}
	}
	user := a.user.GetByEmail(ctx, uservice.GetByEmailIn{Email: in.Email})
	if user.ErrorCode != errors.NoError {
		return RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: userOut.ErrorCode,
		}
	}

	hash := a.hash.GenHashString(nil, cryptography.UUID)
	err = a.verify.Create(ctx, in.Email, hash, user.User.ID)
	if err != nil {
		return RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: http.StatusInternalServerError,
		}
	}
	go a.sendEmailVerifyLink(in.Email, hash)

	return RegisterOut{
		Status:    http.StatusOK,
		ErrorCode: errors.NoError,
	}
}

func (a *Auth) sendEmailVerifyLink(email, hash string) int {
	userOut := a.user.GetByEmail(context.Background(), uservice.GetByEmailIn{Email: email})
	if userOut.ErrorCode != errors.NoError {
		return userOut.ErrorCode
	}

	u, err := url.Parse("http://bing.com/verify?email=sample&hash=sample")
	if err != nil {
		a.logger.Fatal("auth: url parse err", zap.Error(err))

		return errors.AuthUrlParseErr
	}
	u.Scheme = "https"
	if a.conf.Environment != "production" {
		u.Scheme = "http"
	}
	u.Host = a.conf.Domain
	q := u.Query()
	q.Set("email", email)
	q.Set("hash", hash)
	u.RawQuery = q.Encode()
	a.notifyEmail(iservice.PushIn{
		Identifier: email,
		Type:       iservice.PushEmail,
		Title:      "Activation Link",
		Data:       []byte(u.String()),
		Options:    nil,
	})

	return errors.NoError
}

func (a *Auth) notifyEmail(p iservice.PushIn) {
	res := a.notify.Push(p)
	if res.ErrorCode != errors.NoError {
		time.Sleep(1 * time.Minute)
		go a.notifyEmail(p)
	}
}

func (a *Auth) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	userOut := a.user.GetByEmail(ctx, uservice.GetByEmailIn{Email: in.Email})
	if userOut.ErrorCode != errors.NoError {
		return AuthorizeOut{
			ErrorCode: userOut.ErrorCode,
		}
	}
	user := userOut.User
	if !cryptography.CheckPassword(user.Password, in.Password) {
		return AuthorizeOut{
			ErrorCode: errors.AuthServiceWrongPasswordErr,
		}
	}
	if !user.EmailVerified {
		return AuthorizeOut{
			ErrorCode: errors.AuthServiceUserNotVerified,
		}
	}

	accessToken, refreshToken, errorCode := a.generateTokens(user)
	if errorCode != errors.NoError {
		return AuthorizeOut{
			ErrorCode: errorCode,
		}
	}

	return AuthorizeOut{
		UserID:       user.ID,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
}

func (a *Auth) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	userOut := a.user.GetByID(ctx, uservice.GetByIDIn{UserID: in.UserID})
	if userOut.ErrorCode != errors.NoError {
		return AuthorizeOut{
			ErrorCode: userOut.ErrorCode,
		}
	}
	user := userOut.User

	accessToken, refreshToken, errorCode := a.generateTokens(user)
	if errorCode != errors.NoError {
		return AuthorizeOut{
			ErrorCode: errorCode,
		}
	}

	return AuthorizeOut{
		UserID:       user.ID,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
}

func (a *Auth) generateTokens(user *models.User) (string, string, int) {
	accessToken, err := a.tokenManager.CreateToken(
		strconv.Itoa(user.ID),
		strconv.Itoa(user.Role),
		"",
		a.conf.Token.AccessTTL,
		cryptography.AccessToken,
	)
	if err != nil {
		a.logger.Error("auth: create access token err", zap.Error(err))
		return "", "", errors.AuthServiceAccessTokenGenerationErr
	}
	refreshToken, err := a.tokenManager.CreateToken(
		strconv.Itoa(user.ID),
		strconv.Itoa(user.Role),
		"",
		a.conf.Token.RefreshTTL,
		cryptography.RefreshToken,
	)
	if err != nil {
		a.logger.Error("auth: create access token err", zap.Error(err))
		return "", "", errors.AuthServiceRefreshTokenGenerationErr
	}

	return accessToken, refreshToken, errors.NoError
}

func (a *Auth) AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut {
	return AuthorizeOut{}
}

func (a *Auth) SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut {
	panic("asfasf")
}

func (a *Auth) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	dto, err := a.verify.GetByEmail(ctx, in.Email, in.Hash)
	if err != nil {
		return VerifyEmailOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	err = a.verify.VerifyEmail(ctx, dto.GetEmail(), dto.GetHash())
	if err != nil {
		return VerifyEmailOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	out := a.user.VerifyEmail(ctx, uservice.UserVerifyEmailIn{
		UserID: dto.GetUserID(),
	})

	return VerifyEmailOut{
		Success:   out.Success,
		ErrorCode: out.ErrorCode,
	}
}
