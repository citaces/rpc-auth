package service

import (
	"context"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/errors"
	uservice "gitlab.com/citaces/rpc-auth/internal/modules/user/service"
	"log"
	"net/rpc"
)

type AuthServiceJSONRPC struct {
	client *rpc.Client
}

func NewAuthServiceJSONRPC(client *rpc.Client) *AuthServiceJSONRPC {
	a := &AuthServiceJSONRPC{client: client}
	return a
}

func (a *AuthServiceJSONRPC) Register(ctx context.Context, in RegisterIn, field int) RegisterOut {
	var out RegisterOut
	err := a.client.Call("AuthServiceJSONRPC.Register", []interface{}{in, field}, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceUserAlreadyExists
	}
	return out
}

func (a *AuthServiceJSONRPC) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	var out AuthorizeOut
	err := a.client.Call("AuthServiceJSONRPC.AuthorizeEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.NotifyEmailSendErr
	}
	return out
}

func (a *AuthServiceJSONRPC) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	var out AuthorizeOut
	err := a.client.Call("AuthServiceJSONRPC.AuthorizeRefresh", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceRefreshTokenGenerationErr
	}
	return out
}

func (a *AuthServiceJSONRPC) AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut {
	var out AuthorizeOut
	err := a.client.Call("AuthServiceJSONRPC.AuthorizePhone", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceWrongPhoneCodeErr
	}
	return out
}

func (a *AuthServiceJSONRPC) SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut {
	var out SendPhoneCodeOut
	err := a.client.Call("AuthServiceJSONRPC.SendPhoneCode", in, &out)
	if err != nil {
		out.Code = errors.UserServiceWrongPhoneCodeErr
	}
	return out
}

func (a *AuthServiceJSONRPC) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	var out VerifyEmailOut
	err := a.client.Call("AuthServiceJSONRPC.VerifyEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.NotifyEmailSendErr
	}
	return out
}

func (a *AuthServiceJSONRPC) SetUserer(user uservice.Userer) {
	err := a.client.Call("AuthServiceJSONRPC.SetUserer", user, nil)
	if err != nil {
		log.Fatalf("AuthServiceJSONRPC SetUserer error %v", err)
	}
}
