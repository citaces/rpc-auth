package modules

import (
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/component"
	aservice "gitlab.com/citaces/rpc-auth/internal/modules/auth/service"
	uservice "gitlab.com/citaces/rpc-auth/internal/modules/user/service"
	"gitlab.com/citaces/rpc-auth/internal/storages"
	"net/rpc"
)

type Services struct {
	User          uservice.Userer
	Auth          aservice.Auther
	UserClientRPC uservice.Userer
}

func NewServices(client *rpc.Client, storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserServiceJSONRPC(client)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
