package service

import (
	"context"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/errors"
	"net/rpc"
)

type UserServiceJSONRPC struct {
	client *rpc.Client
}

func NewUserServiceJSONRPC(client *rpc.Client) *UserServiceJSONRPC {
	u := &UserServiceJSONRPC{client: client}

	return u
}

func (t *UserServiceJSONRPC) Create(ctx context.Context, in UserCreateIn) UserCreateOut {
	var out UserCreateOut
	err := t.client.Call("UserServiceJSONRPC.CreateUser", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceCreateUserErr
	}

	return out
}

func (t *UserServiceJSONRPC) Update(ctx context.Context, in UserUpdateIn) UserUpdateOut {
	var out UserUpdateOut
	err := t.client.Call("UserServiceJSONRPC.UpdateUser", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceUpdateErr
	}

	return out
}

func (t *UserServiceJSONRPC) VerifyEmail(ctx context.Context, in UserVerifyEmailIn) UserUpdateOut {
	var out UserUpdateOut
	err := t.client.Call("UserServiceJSONRPC.VerifyEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceUpdateErr
	}

	return out
}

func (t *UserServiceJSONRPC) ChangePassword(ctx context.Context, in ChangePasswordIn) ChangePasswordOut {
	var out ChangePasswordOut
	err := t.client.Call("UserServiceJSONRPC.ChangePassword", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceChangePasswordErr
	}

	return out
}

func (t *UserServiceJSONRPC) GetByEmail(ctx context.Context, in GetByEmailIn) UserOut {
	var out UserOut
	err := t.client.Call("UserServiceJSONRPC.GetUserByEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
	}

	return out
}

func (t *UserServiceJSONRPC) GetByPhone(ctx context.Context, in GetByPhoneIn) UserOut {
	var out UserOut
	err := t.client.Call("UserServiceJSONRPC.GetUserByPhone", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
	}

	return out
}

func (t *UserServiceJSONRPC) GetByID(ctx context.Context, in GetByIDIn) UserOut {
	var out UserOut
	err := t.client.Call("UserServiceJSONRPC.GetUserByID", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
	}
	return out
}

func (t *UserServiceJSONRPC) GetByIDs(ctx context.Context, in GetByIDsIn) UsersOut {
	var out UsersOut
	err := t.client.Call("UserServiceJSONRPC.GetUsersByID", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUsersErr
	}
	return out
}
