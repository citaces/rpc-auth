package db

import (
	"database/sql"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/citaces/rpc-auth/config"
	"gitlab.com/citaces/rpc-auth/internal/db/adapter"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/db/scanner"
	"go.uber.org/zap"
	"time"
)

func NewSqlDB(dbConf config.DB, scanner scanner.Scanner, logger *zap.Logger) (*sqlx.DB, *adapter.SQLAdapter, error) {
	var dsn string
	var err error
	var dbRaw *sql.DB

	switch dbConf.Driver {
	case "postgres":
		dsn = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbConf.Host, dbConf.Port, dbConf.User, dbConf.Password, dbConf.Name)
	case "mysql":
		cfg := mysql.NewConfig()
		cfg.Net = dbConf.Net
		cfg.Addr = dbConf.Host
		cfg.User = dbConf.User
		cfg.Passwd = dbConf.Password
		cfg.DBName = dbConf.Name
		cfg.ParseTime = true
		cfg.Timeout = time.Duration(dbConf.Timeout) * time.Second
		dsn = cfg.FormatDSN()
	case "ramsql":
		dsn = "Testing"
	}

	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	timeoutExceeded := time.After(time.Second * time.Duration(dbConf.Timeout))

	for {
		select {
		case <-timeoutExceeded:
			return nil, nil, fmt.Errorf("db connection failed after %d timeout %s", dbConf.Timeout, err)
		case <-ticker.C:
			dbRaw, err = sql.Open(dbConf.Driver, dsn)
			if err != nil {
				return nil, nil, err
			}
			err = dbRaw.Ping()
			if err == nil {
				db := sqlx.NewDb(dbRaw, dbConf.Driver)
				db.SetMaxOpenConns(50)
				db.SetMaxIdleConns(50)
				sqlAdapter := adapter.NewSqlAdapter(db, dbConf, scanner)
				return db, sqlAdapter, nil
			}
			logger.Error("failed to connect to the database", zap.String("dsn", dsn), zap.Error(err))
		}
	}
}
