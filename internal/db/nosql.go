package db

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/citaces/rpc-auth/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

const (
	dsnFmt = "mongodb://%s:%s@%s:%s/%s?retryWrites=true&w=majority&authSource=admin"
)

type Collection interface {
	GetCollection(collectionName string) *mongo.Collection
}

type MongoCollection struct {
	client *mongo.Client
	conf   config.DB
}

func (m *MongoCollection) GetCollection(collectionName string) *mongo.Collection {
	return m.client.Database(m.conf.Name).Collection(collectionName)
}

func NewMongoCollection(conf config.DB, logger *zap.Logger) Collection {
	dsn := fmt.Sprintf(dsnFmt, conf.User, conf.Password, conf.Host, conf.Port, conf.Name)
	client, err := mongo.NewClient(options.Client().ApplyURI(dsn))
	if err != nil {
		logger.Fatal("mongodb create client err", zap.Error(err))
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(1))
	defer cancel()
	err = client.Connect(ctx)
	if err == nil {
		return &MongoCollection{
			client: client,
			conf:   conf,
		}
	}
	//ping the database
	err = client.Ping(context.Background(), nil)
	if err == nil {
		return &MongoCollection{
			client: client,
			conf:   conf,
		}
	}

	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	timeoutExceeded := time.After(time.Second * time.Duration(conf.Timeout))

	for {
		select {
		case <-timeoutExceeded:
			logger.Fatal("connection timeout", zap.Error(fmt.Errorf("db connection failed after %d timeout", conf.Timeout)), zap.Error(err))
		case <-ticker.C:
			ctx, cancel = context.WithTimeout(context.Background(), time.Second*time.Duration(1))
			defer cancel()
			err = client.Connect(ctx)
			if err != nil {
				continue
			}
			//ping the database
			err = client.Ping(ctx, nil)
			if err == nil {
				return &MongoCollection{
					client: client,
					conf:   conf,
				}
			}
			logger.Error("failed to connect to the database", zap.String("dsn", dsn), zap.Error(err))
		}
	}
}
