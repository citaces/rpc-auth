package storages

import (
	"gitlab.com/citaces/rpc-auth/internal/db/adapter"
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/cache"
	vstorage "gitlab.com/citaces/rpc-auth/internal/modules/auth/storage"
	ustorage "gitlab.com/citaces/rpc-auth/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
