package server

import (
	"context"
	"fmt"
	"gitlab.com/citaces/rpc-auth/config"
	"go.uber.org/zap"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
)

type ServerJSONRPC struct {
	conf   config.RPCServer
	logger *zap.Logger
	srv    *rpc.Server
}

func NewJSONRPC(conf config.RPCServer, srv *rpc.Server, logger *zap.Logger) Server {
	return &ServerJSONRPC{conf: conf, logger: logger, srv: srv}
}

func (s *ServerJSONRPC) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		var l net.Listener
		l, err = net.Listen("tcp", fmt.Sprintf(":%s", s.conf.Port))
		if err != nil {
			s.logger.Error("json rpc server register error", zap.Error(err))
			chErr <- err
		}

		s.logger.Info("json rpc server started", zap.String("port", s.conf.Port))
		var conn net.Conn
		for {
			select {
			case <-ctx.Done():
				s.logger.Error("json rpc: stopping server")
				return
			default:
				var connErr error
				conn, connErr = l.Accept()
				if err != nil {
					s.logger.Error("json rpc: net tcp accept", zap.Error(connErr))
				}
				go s.srv.ServeCodec(jsonrpc.NewServerCodec(conn))
			}
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}

	return err
}
