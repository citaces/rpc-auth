package models

import (
	"gitlab.com/citaces/rpc-auth/internal/infrastructure/db/types"
	"time"
)

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index

type TestDTO struct {
	ID             int              `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null" db_ops:"id" db_index:"id"`
	TestNullString types.NullString `json:"test_null_string" db:"test_null_string" db_ops:"create,update" db_type:"varchar(144)" db_default:"default null"`
	IntField       int              `json:"int_field" db:"int_field" db_ops:"create,update" db_type:"int" db_default:"default 0"`
	IntNull        types.NullInt64  `json:"int_null" db:"int_null" db_ops:"create,update" db_type:"int" db_default:"default null"`
	BoolField      bool             `json:"bool_field" db:"bool_field" db_ops:"create,update" db_type:"bool" db_default:"default false"`
	CreatedAt      time.Time        `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt      time.Time        `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt      types.NullTime   `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

func (u *TestDTO) TableName() string {
	return "test"
}

func (u *TestDTO) OnCreate() []string {
	return []string{}
}
